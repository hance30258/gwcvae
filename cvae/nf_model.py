import numpy as np
import tensorflow as tf
import tensorflow_probability as tfp

tfkl = tf.keras.layers
tfpl = tfp.layers
tfd = tfp.distributions
tfb = tfp.bijectors

from cvae.tf_utils import network_make, model_make

xdim = 5
ydim = [512,2]
pdim = [512,1]
zdim = 8
ldim = 2048

class CVAE(tf.keras.Model):
	def __init__(self, config):
		super(CVAE, self).__init__()
		print("Create CVAE")


		Y = tfkl.Input( shape=ydim, name="Y")
		X = tfkl.Input( shape=(xdim,), name="X")
		PSD = tfkl.Input( shape=pdim, name="PSD")
		FlateP = tfkl.Flatten()(PSD)
		FC1P =	tfkl.Dense( units=ldim, activation="relu", kernel_initializer="GlorotNormal")(FlateP)
		FC2P =	tfkl.Dense( units=ldim, activation="relu", kernel_initializer="GlorotNormal")(FC1P)
		FC3P =	tfkl.Dense( units=ldim, activation="relu", kernel_initializer="GlorotNormal")(FC2P)

		FlateY = tfkl.Flatten()(Y)
		YP	=	tfkl.Concatenate( axis=1)([FlateY,FC3P])
		FC1 =	tfkl.Dense( units=ldim, activation="relu", kernel_initializer="GlorotNormal")(FlateY)
		FC2 =	tfkl.Dense( units=ldim, activation="relu", kernel_initializer="GlorotNormal")(FC1)
		FC3 =	tfkl.Dense( units=ldim, activation="relu", kernel_initializer="GlorotNormal")(FC2)
		mu	=	tfkl.Dense( units=xdim, kernel_initializer="GlorotNormal")(FC3)
		var	=	tfkl.Dense( units=xdim, kernel_initializer="GlorotNormal")(FC3)
		scale = tfb.Scale( log_scale = tf.exp(-var))
		shift = tfb.Shift( shift = mu)

		made1=	tfb.MaskedAutoregressiveFlow( tfb.AutoregressiveNetwork(
				params=2,
				hidden_units=[512, 512],
				event_shape=xdim,
				input_order="random",
				), name="maf1")
		made2=	tfb.MaskedAutoregressiveFlow( tfb.AutoregressiveNetwork(
				params=2,
				hidden_units=[512, 512],
				event_shape=xdim,
				input_order="random",
				), name="maf2")
		made3=	tfb.MaskedAutoregressiveFlow( tfb.AutoregressiveNetwork(
				params=2,
				hidden_units=[512, 512],
				event_shape=xdim,
				input_order="random",
				), name="maf3")

		chain = tfb.Chain([
			scale, 
			shift, 
			made1, 
			made2,
			made3,])

		distribution = tfd.TransformedDistribution(
				distribution=tfd.Sample(tfd.Normal(loc=0., scale=1.), sample_shape=[xdim]),
				bijector=chain)

		logpx = distribution.log_prob( X)
		sample = distribution.sample(1000)

		E1 = tf.keras.Model( [Y,X,PSD], logpx)
		sample = tf.keras.Model( [Y,PSD], sample)

		self.E1 = E1
		self.sample = sample



	def choose_input(self, network, data):
		return dict( (k, data[k]) for k in network.input_names if k in data)

	@tf.function
	def loss(self, data):
		
		logpx = self.E1( self.choose_input( self.E1, data))

		return -tf.reduce_mean( logpx)

	@tf.function
	def test(self, data, N=2):
		sample = self.sample( self.choose_input( self.sample, data))

		return sample


@tf.function
def training( optimizer, model, data, beta):

	with tf.GradientTape() as tape:
		loss = model.loss( data)
		gradients = tape.gradient( loss, model.trainable_variables)

	# gradients cutoff
	#gradients = [(tf.clip_by_value(grad, -1.0, 1.0))
	#		for grad in gradients]

	optimizer.apply_gradients(zip(gradients, model.trainable_variables))
	return loss, loss

