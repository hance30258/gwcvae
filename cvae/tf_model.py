import tensorflow as tf
import tensorflow_probability as tfp

tfkl = tf.keras.layers
tfpl = tfp.layers

from cvae.tf_utils import model_make


class CVAE(tf.keras.Model):
	def __init__(self, config):
		super(CVAE, self).__init__()
		print("Create CVAE")
		# Create DL Model
		for name, net in config["network"].items():
			setattr( self, name, model_make(name, net))

	# Choose input of model as dictionary
	def choose_input(self, network, data):
		return dict( (k, data[k]) for k in network.input_names if k in data)

	@tf.function
	def loss(self, data):
		
		P_XY = self.E1( self.choose_input( self.E1, data))
		P_Y = self.E2( self.choose_input( self.E2, data))

		Z = P_XY.sample()
		input_D1 = self.choose_input( self.D1, data)
		input_D1["Z"] = Z

		P_YZ = self.D1( input_D1)

		logpx = P_YZ.log_prob( data["X"])
		#kl_div = tfp.distributions.kl_divergence( P_XY, P_Y, allow_nan_stats=True)
		#if float('nan') in kl_div:
		kl_div = tfp.monte_carlo.expectation(
				f=lambda x: P_XY.log_prob(x) - P_Y.log_prob(x),
				samples=P_XY.sample(),
				log_prob=P_XY.log_prob,
				#use_reparameterization=( P_XY.reparameterization_type ==tfd.FULLY_REPARAMETERIZATION),
				)

		return -tf.reduce_mean( logpx), tf.reduce_mean( kl_div)

	@tf.function
	def crossentropy(self, data, N=1024):

		P_Y = self.E2( self.choose_input( self.E2, data))

		input_D1 = self.choose_input( self.D1, data)
		for k in input_D1.keys():
			input_D1[k] = tf.repeat( input_D1[k], repeats=N, axis=0) 
		input_D1["Z"] = tf.squeeze( P_Y.sample(N))

		P_YZ = self.D1( input_D1)


		X_repeat = tf.repeat( data["X"], repeats=N, axis=0)
		px = P_YZ.prob( X_repeat)
		px_reshape = tf.reduce_mean( tf.reshape( px, [ data["X"].shape[0], N, -1]), axis=1)

		return -tf.reduce_mean(tf.math.log(px_reshape))

	@tf.function
	def test(self, data, N=2):
		P_Y = self.E2( self.choose_input( self.E2, data))

		input_D1 = self.choose_input( self.D1, data)
		for k in input_D1.keys():
			input_D1[k] = tf.repeat( input_D1[k], repeats=N, axis=0) 
		input_D1["Z"] = tf.squeeze( P_Y.sample(N))

		P_YZ = self.D1( input_D1)

		return P_YZ.mean()


@tf.function
def training( optimizer, model, data, beta):

	with tf.GradientTape() as tape:
		logpx, kl_div = model.loss( data)
		if kl_div > -logpx+10:
			beta = 1.
		#if -kl_div/logpx < 1./5:
		#	loss = logpx
		#else:
		#	loss = logpx + kl_div
		loss = logpx + kl_div * beta
		gradients = tape.gradient( loss, model.trainable_variables)

	# gradients cutoff
	gradients = [(tf.clip_by_value(grad, -0.1, 0.1))
			for grad in gradients]

	optimizer.apply_gradients(zip(gradients, model.trainable_variables))
	return logpx, kl_div
