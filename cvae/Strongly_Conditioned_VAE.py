import numpy as np
import tensorflow as tf
import tensorflow_probability as tfp

tfkl = tf.keras.layers
tfpl = tfp.layers

from cvae.tf_utils import network_make, model_make


class CVAE(tf.keras.Model):
	def __init__(self, config):
		super(CVAE, self).__init__()
		print("Create CVAE")
		for name, net in config["network"].items():
			setattr( self, name, model_make(name, net))

	def choose_input(self, network, data):
		return dict( (k, data[k]) for k in network.input_names if k in data)

	@tf.function
	def loss(self, data):
		
		MU_zc, SIG_zc = self.E2( self.choose_input( self.E2, data))
		MU_z, SIG_z = self.E1( self.choose_input( self.E1, data))
		SIG_zc = tf.math.exp(-SIG_zc)
		SIG_z = tf.math.exp(-SIG_z)

		P_Zc = tfp.distributions.MultivariateNormalDiag(loc=MU_zc,scale_diag=SIG_zc)
		P_Z = tfp.distributions.MultivariateNormalDiag(
				loc= MU_z+SIG_z*MU_zc,
				scale_diag=SIG_z*SIG_zc)

		Zc = P_Zc.sample()
		Z = P_Z.sample()

		input_D1 = self.choose_input( self.D1, data)
		input_D1["Z"] = Z
		MU_x, SIG_x = self.D1( input_D1)
		SIG_x = tf.math.exp(-SIG_x)
		P_x = tfp.distributions.MultivariateNormalDiag(loc=MU_x,scale_diag=SIG_x)

		input_D2 = self.choose_input( self.D2, data)
		input_D2["Zc"] = Zc
		y_pred = self.D2( input_D2)

		# reconstruction for D1 & D2
		logpx = P_x.log_prob( data["X"])
		#py_loss = tf.keras.losses.MSE( tf.reshape(data["Y"], [512,-1]), y_pred)
		py_loss = tf.keras.losses.MSE( data["Y"], y_pred)
		# KL divergence for E1 & E2
		kl_div_prior = P_Zc.kl_divergence( tfp.distributions.MultivariateNormalDiag( loc=tf.zeros(8), scale_diag=tf.ones(8)))
		kl_div = tfp.distributions.kl_divergence( P_Z, P_Zc)

		return -tf.reduce_mean( logpx)+tf.reduce_mean(py_loss), tf.reduce_mean( kl_div+kl_div_prior)

	@tf.function
	def test(self, data, N=2):
		MU_zc, SIG_zc = self.E2( self.choose_input( self.E2, data))
		SIG_zc = tf.math.exp(-SIG_zc)

		P_Zc = tfp.distributions.MultivariateNormalDiag(loc=MU_zc,scale_diag=SIG_zc)

		#input_D1 = dict( (k, tf.repeat( data[k], repeats=N, axis=0)) for k in self.D1.input_names if k in data)
		input_D1 = self.choose_input( self.D1, data)
		for key in input_D1.keys():
			input_D1[key] = tf.repeat( input_D1[key], repeats=N, axis=0)
		input_D1["Z"] = tf.squeeze( P_Zc.sample(N))

		MU_x, SIG_x = self.D1( input_D1)
		SIG_x = tf.math.exp(-SIG_x)
		P_x = tfp.distributions.MultivariateNormalDiag(loc=MU_x,scale_diag=SIG_x)

		return P_x.sample()


@tf.function
def training( optimizer, model, data, beta):

	with tf.GradientTape() as tape:
		logpx, kl_div = model.loss( data)
		loss = logpx + kl_div * beta
		gradients = tape.gradient( loss, model.trainable_variables)

	# gradients cutoff
	#gradients = [(tf.clip_by_value(grad, -1.0, 1.0))
	#		for grad in gradients]

	optimizer.apply_gradients(zip(gradients, model.trainable_variables))
	return logpx, kl_div
