import h5py as h5
import tensorflow as tf
import tensorflow_probability as tfp

tfkl = tf.keras.layers
tfpl = tfp.layers

from cvae.tf_utils import network_make


def model_make( model_name, network):

	net = []
	reuse_layer = dict()
	io = dict()
	for name, dicts in network["layer"].items():
		print( name, dicts)


		# Save layers if reuse
		if "reuse" in dicts.keys():
			layer = reuse_layer[ dicts["reuse"]]
		else:
			# Create layers
			layer = network_make( name, dicts)
			reuse_layer[name] = layer

		# Get upper layers
		g = dicts["group"]
		if isinstance( g, list):
			up = [ net[i] for i in g]
			l = layer(up)
			net.append( l)
		elif g < len(net):
			l = layer(net[g])
			if "split" in dicts.keys():
				net.append( l)
			else:
				net[g] = l
		else:
			l = layer
			net.append( layer)

		for args in ["inputs","outputs"]:
			if args in dicts.keys():
				for model_name in dicts[args]:
					if model_name not in io.keys():
						io[model_name] = dict( inputs=[], outputs=[])
					io[model_name][args].append( l)
					#print( model_name, args, l)

	model = dict()
	for model_name in io.keys():
		#print( io[model_name])
		model[model_name] = tf.keras.Model( inputs=io[model_name]["inputs"], outputs=io[model_name]["outputs"], name=model_name)
	return model


class CVAE(tf.keras.Model):
	def __init__(self, config):
		super(CVAE, self).__init__()
		print("Create CVAE")
		# Create DL Model
		for name, net in config["network"].items():
			model = model_make(name, net)
			self._model_name = model.keys()
			for model_name, Network in model.items():
				setattr( self, model_name, Network)
				Network.summary()
				tf.keras.utils.plot_model( Network, to_file=f"{model_name}.png", show_shapes=True)

	#def load_weights(self, filename):
	#	with h5.File( filename, "r") as f:
	#		for name in self._model_name:
	#			for variable in getattr( self, name).trainable_variables:
	#				if variable.name in f:
	#					print( "Load,", variable.name, variable.numpy().shape)
	#					variable = f[ variable.name][()]
	#				else:
	#					print( "No weights found, skip ", variable.name, variable.numpy().shape)

	#def save_weights(self, filename):
	#	with h5.File( filename, "w") as f:
	#		for name in self._model_name:
	#			for variable in getattr( self, name).trainable_variables:
	#				if variable.name not in f:
	#					print( "Save,", variable.name, variable.numpy().shape)
	#					f[ variable.name] = variable.numpy()


	# Choose input of model as dictionary
	def choose_input(self, network, data):
		return dict( (k, data[k]) for k in network.input_names if k in data)

	@tf.function
	def loss(self, data):
		
		P_XY, P_Y, P_YZ = self.elbo_network( self.choose_input( self.elbo_network, data))

		logpx = P_YZ.log_prob( data["X"])

		kl_div = tfp.monte_carlo.expectation(
				f=lambda x: P_XY.log_prob(x) - P_Y.log_prob(x),
				samples=P_XY.sample(),
				log_prob=P_XY.log_prob,
				#use_reparameterization=( P_XY.reparameterization_type ==tfd.FULLY_REPARAMETERIZATION),
				)

		return -tf.reduce_mean( logpx), tf.reduce_mean( kl_div)

	@tf.function
	def crossentropy(self, data, N=128):

		input_sample = self.choose_input( self.sample_network, data)
		for k in input_sample.keys():
			input_sample[k] = tf.repeat( input_sample[k], repeats=N, axis=0) 

		P_YZ = self.sample_network( input_sample)


		X_repeat = tf.repeat( data["X"], repeats=N, axis=0)
		px = P_YZ.prob( X_repeat)
		px_reshape = tf.reduce_mean( tf.reshape( px, [ data["X"].shape[0], N, -1]), axis=1)

		return -tf.reduce_mean(tf.math.log(px_reshape))

	@tf.function
	def test(self, data, N=2):

		input_sample = self.choose_input( self.sample_network, data)
		for k in input_sample.keys():
			input_sample[k] = tf.repeat( input_sample[k], repeats=N, axis=0) 

		P_YZ = self.sample_network( input_sample)

		return P_YZ.mean()


@tf.function
def training( optimizer, model, data, beta):

	with tf.GradientTape() as tape:
		logpx, kl_div = model.loss( data)
		loss = logpx + kl_div * beta
		gradients = tape.gradient( loss, model.trainable_variables)

	# gradients cutoff
	#gradients = [(tf.clip_by_value(grad, -1., 1.))
	#		for grad in gradients]

	optimizer.apply_gradients(zip(gradients, model.trainable_variables))
	return logpx, kl_div

