import tensorflow as tf
import tensorflow_probability as tfp

tfkl = tf.keras.layers
tfpl = tfp.layers

def network_make( name, para, up=None):
	if hasattr( tfkl, para["net"]):
		layer = getattr( tfkl, para["net"])
	elif hasattr( tfpl, para["net"]):
		layer = getattr( tfpl, para["net"])
	else:
		print( f"No layers found:::{para['net']}")
		exit(1)
	if "function" in para["args"]:
		para["args"]["function"] = getattr( custom_function, para["args"]["function"])
		net = layer( **para["args"], name=name)

	else:
		if up == None:
			print( f"net[{name}] ,", para["args"])
			net = layer( **para["args"], name=name)
		else:
			net = layer( **para["args"], name=name)(up)

	return net

def model_make( model_name, network):

	net = []
	inputs = []
	outputs = []
	for name, dicts in network["layer"].items():

		g = dicts["group"]
		if isinstance( g, list):
			up = []
			for ind in g:
				up.append(net[ind])
			l = network_make( name, dicts, up)
		elif g < len(net):
			l = network_make( name, dicts, net[g])
			if not "split" in dicts.keys():
				net[g] = l
				continue
		else:
			l = network_make( name, dicts)
			inputs.append(l)

		if "output" in dicts.keys():
			outputs.append(l)
		net.append(l)
	#print( inputs)
	#print( outputs)
	model = tf.keras.Model( inputs=inputs, outputs=outputs, name=model_name)
	model.summary()
	return model

class custom_function():
	def minus_relu(x):
		return -tf.keras.activations.relu(x)
	def minus_exp(x):
		return tf.keras.activations.exponential(-x)

#	*	*	*	*	*	*	*	*	
#	TODO
#	Is training function needed?
#	Change this file as model_utils and generalize it for all CVAE models
#	
#	*	*	*	*	*	*	*	*	
