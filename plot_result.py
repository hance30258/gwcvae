#	*	*	*	*	*	*	*	*	
#	Training code
#	*	*	*	*	*	*	*	*	
import random
random.seed(10)
import argparse
import glob, os
import json

parser = argparse.ArgumentParser(description="training code")
parser.add_argument('configure')
args = parser.parse_args()
print(f"cfg:\t{args.configure}")

#	Read configure file
with open( args.configure,"r") as jfile:
	config = json.load( jfile)

# import module
import importlib
tf_model = importlib.import_module(config["cvae_model"]["use_model"])
waveform_module = importlib.import_module(config["waveform_model"]["use_model"])


#	Read Training data
fn = glob.glob( config['waveform_model']['savepath']+'/*.h5')
fn.sort()
print("file",fn)
#	split training set
valid_file = fn[::5].copy()
print("valid_file",valid_file)
train_file = fn.copy()
for item in valid_file:
	train_file.remove(item)

print("train_file",train_file)

waveform = waveform_module.waveform
#wf_train = waveform( config["waveform_model"]["parameter"])
wf_valid = waveform( config["waveform_model"]["parameter"])

#wf_train.readdata( train_file)
wf_valid.readdata( valid_file)

#print( wf_train.data["S"].shape)
print( wf_valid.data["S"].shape)


import numpy as np
import matplotlib.pyplot as plt
from sklearn import preprocessing

def preprocessing_scaler( wf, batch=512):
	#data = wf.random_batch( len(wf.data["X"]))
	data = wf.random_batch( batch*100)
	scaler = dict()
	for key in config["preprocessing"].keys():
		process = getattr( preprocessing, config["preprocessing"][key]["method"])
		shape = data[key].shape
		print(f"{key}\t:\t{shape}")
		if data[key].dtype == np.complex64:
			data[key] = np.concatenate( [ data[key].real, data[key].imag], axis=1)
			scaler[key] = process( **config["preprocessing"][key]["args"]).fit( data[key].reshape([-1,1]))
		else:
			scaler[key] = process( **config["preprocessing"][key]["args"]).fit( data[key])
		print( f"parameters\t:\t{scaler[key].get_params()}")
	return scaler

def preprocessing_batch( data, scaler):
	for key in config["preprocessing"].keys():
		if data[key].dtype == np.complex64:
			shape = data[key].shape
			data[key] = np.concatenate( [ data[key].real, data[key].imag], axis=1)
			data[key] = scaler[key].transform( data[key].reshape(-1,1))
			data[key] = data[key].reshape([shape[0],shape[1],2]).astype("float32")
			data[key] /= 4
		else:
			data[key] = scaler[key].transform( data[key]).astype("float32")

#scaler_train = preprocessing_scaler( wf_train)
scaler_valid = preprocessing_scaler( wf_valid)

#exit()



#	Create model
model = tf_model.CVAE( config["cvae_model"])


#	load latest weights
wfile = glob.glob( config["cvae_model"]["savepath"]+"/*.h5")
wfile.sort(key=os.path.getmtime)
print( wfile)
if wfile:
	model.built = True
	model.load_weights( wfile[-1])
	print(f"Read weights\t:\t{wfile[-1]}")

import corner

for i in range(10):
	batch_data = wf_valid.random_batch( batch=1)
	plt.figure(figsize=(32,18))
	plt.plot(batch_data["Y"][0].real)
	plt.plot(batch_data["S"][0].real)
	plt.savefig(f"figure{i}_y.png")
	plt.clf()
	preprocessing_batch( batch_data, scaler_valid)
	
	for j in range(1):
		sample = model.test( batch_data, N=20000).numpy().astype(np.float64)
		sample = scaler_valid["X"].inverse_transform( sample)	
		if j ==0:
			arr = sample
			continue
		arr = np.concatenate( [arr, sample], axis=0)
	print(arr.shape)
	
	figure = corner.corner( arr)
	
	plt.figure(figsize=(32,18))
	plt.savefig(f"figure{i}.png")
	plt.clf()
