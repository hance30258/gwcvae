import argparse
import os

parser = argparse.ArgumentParser(description="dataset generate code")
parser.add_argument('--test', action='store_true')
parser.add_argument('--pe', help='pe folder number')
parser.add_argument('--length', help='length')
parser.add_argument('--num', help='num')
parser.add_argument('--gpstime', help='gpstime')
parser.add_argument('--strain', help='strain')
parser.add_argument('configure')
parser.add_argument('outfile')
args = parser.parse_args()

import matplotlib.pyplot as plt
import numpy as np
import h5py as h5
import pycbc.noise
import pycbc.psd
from gwpy.timeseries import TimeSeries
import bilby
import requests, os


o3e = np.genfromtxt('o3event.txt',dtype=str)


fname = os.path.abspath(args.configure)
event_time = [
		#float(args.gpstime),
		float(o3e[ int(args.num)][2])
		#1242459857.5,    # GW190521_074359
		]

event_name = o3e[ int(args.num)][0]
print( event_time, event_name)
#exit()


import json
with open( args.configure,"r") as jfile:
	config = json.load( jfile)

config = config["waveform_model"]

import importlib
waveform_module = importlib.import_module(config["use_model"])
waveform = waveform_module.waveform

wf = waveform( config["parameter"])

plot = True

detector = wf.detector[0]
#detector = 'H1'

fs = wf.fs

from gwosc.locate import get_urls, get_event_urls

for t0 in event_time:
	segm = 7000
	#gps = 1268903511.3
	#url = get_urls(detector, t0, t0)[0]
	url = get_event_urls(event=event_name, duration=4096, detector=detector)[0]
	#url = get_urls(detector, gps-segm, gps+segm)[0]
	fn = os.path.basename(url)
	print(fn)
	#exit()
	#fn = args.strain

	if not os.path.exists(fn):
		print(f"Downloading\t{fn}")
		with open(fn,'wb') as strainfile:
			straindata = requests.get(url)
			strainfile.write(straindata.content)
	strain = TimeSeries.read(fn,format='hdf5.losc')
	center = int(float(t0))
	length = int(float(args.length))
	strain = strain.crop(center-length//2, center+length//2)
	psd = strain.psd(fftlength=wf.duration, overlap=0.5,method='median')
	strain = strain.resample(fs)
	print(strain.sample_rate)
	#exit()

	ifos = bilby.gw.detector.InterferometerList([detector])
	tc = 0.75

	if plot:
		hq = strain.q_transform(outseg=(t0-tc-wf.duration+1,t0+1-tc))
		fig4 = hq.plot()
		ax = fig4.gca()
		fig4.colorbar(label="Normalised energy")
		ax.grid(False)
		ax.set_yscale('log')
		plt.savefig(f"{args.pe}_{t0}.jpg")
		plt.clf()
		#exit()
	
	pe_strain = strain.crop(t0-tc-wf.duration+1,t0+1-tc)
	pe_strain.epoch = wf.gpstime
	print( pe_strain.epoch)
	#ifos[0].strain_data.roll_off = 0.1
	ifos[0].strain_data.set_from_gwpy_timeseries( pe_strain)
	ifos[0].power_spectral_density = bilby.gw.detector.PowerSpectralDensity.from_power_spectral_density_array(
					frequency_array=psd.frequencies,
					psd_array=psd)

	freq_strain =ifos[0].strain_data.frequency_domain_strain
	psd_strain =ifos[0].amplitude_spectral_density_array/ifos[0].strain_data.window_factor
	wf.data = dict( S=freq_strain.reshape(1,-1), ASD=psd_strain.reshape(1,-1))
	wf.savedata(args.outfile)


	#exit()

	# Get injection parameters
	injection = wf.injection

	# Fixed arguments passed into the source model
	waveform_arguments = dict(waveform_approximant=wf.Approximant,
			reference_frequency=50., minimum_frequency=wf.flow)

	# Create the waveform_generator using a LAL BinaryBlackHole source function
	waveform_generator = bilby.gw.WaveformGenerator(
			duration=wf.duration, sampling_frequency=wf.fs,
			frequency_domain_source_model=bilby.gw.source.lal_binary_black_hole,
			parameter_conversion=bilby.gw.conversion.convert_to_lal_binary_black_hole_parameters,
			waveform_arguments=waveform_arguments)

	# Set prior 
	PriorDict = bilby.core.prior.PriorDict()
	for key in wf.Prior.keys():
		PriorDict[key] = getattr( bilby.core.prior, wf.Prior[key]["distribution"])( **wf.Prior[key]["args"])
	if hasattr( wf, 'extrinsic'):
		for key in wf.extrinsic.keys():
			PriorDict[key] = getattr( bilby.core.prior, wf.extrinsic[key]["distribution"])( **wf.extrinsic[key]["args"])
	
	priors = bilby.gw.prior.BBHPriorDict()
	for key in injection.keys():
		priors[key] = injection[key]
	for key in PriorDict.keys():
		priors[key] = PriorDict[key]
	
	#priors['geocent_time'] = bilby.core.prior.Uniform(
	#    minimum=injection["geocent_time"] - 0.1,
	#    maximum=injection["geocent_time"] + 0.1)
	#priors['mass_1'] = bilby.core.prior.Constraint( **wf.Prior["mass_1"]["args"])
	#priors['mass_2'] = bilby.core.prior.Constraint( **wf.Prior["mass_2"]["args"])
	# Initialise the likelihood by passing in the interferometer data (ifos) and
	# the waveform generator
	likelihood = bilby.gw.GravitationalWaveTransient(
			interferometers=ifos, waveform_generator=waveform_generator)

	#sampler = "ptemcee"
	sampler = "dynesty"
	num = event_time[0]
	outdir = f"{args.pe}_{sampler}_{num}"
	bilby.core.utils.setup_logger(outdir=outdir, label=outdir)
	# Run sampler.  In this case we're going to use the `dynesty` sampler
	result = bilby.run_sampler(
			likelihood=likelihood, priors=priors, 
			sampler='dynesty', 
			nlive=5000,
			walks=30,
			n_check_point=10000,
			npool=2,
			injection_parameters=injection, outdir=outdir, label=outdir,
			save='hdf5')
	##result = bilby.run_sampler(
	##    likelihood=likelihood, priors=priors, sampler='ptemcee', nwalker=800, ntemps=28, npool=28,
	##    injection_parameters=injection, outdir=outdir, label=outdir,
	##    save='hdf5')

	## Make a corner plot.
	result.plot_corner()



	#print(psd_strain)
	#plt.loglog( psd.frequencies, psd_strain)
	#plt.loglog( psd.frequencies, np.sqrt(psd))
	#plt.plot( np.linspace(0,4096//2,len(freq_strain)), freq_strain.data)
#plt.xlim(20)
#plt.show()
#
#plt.plot( psd.frequencies, psd_strain/np.sqrt(psd))
#plt.show()



exit()
