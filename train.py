#	*	*	*	*	*	*	*	*	
#	Training code
#	*	*	*	*	*	*	*	*	

import argparse
import glob, os
import json

parser = argparse.ArgumentParser(description="training code")
parser.add_argument('configure', help="configure file")
parser.add_argument("-t", "--training-data", help="training data", dest="tdata")
parser.add_argument("-v", "--validation-data", help="validation data", dest="vdata")
parser.add_argument("-o", "--output", help="final weight output", dest="output", default="final_weight.h5")
args = parser.parse_args()
print(f"cfg:\t{args.configure}")

output = os.path.abspath( args.output)
#	Read configure file
with open( args.configure,"r") as jfile:
	config = json.load( jfile)

import tensorflow as tf
mirrored_strategy = tf.distribute.MirroredStrategy()


# import module
import importlib

#	Create model
tf_model = importlib.import_module(config["cvae_model"]["use_model"])
with mirrored_strategy.scope():
	model = tf_model.CVAE( config["cvae_model"])

#	Create waveform model
waveform_module = importlib.import_module(config["waveform_model"]["use_model"])

waveform = waveform_module.waveform
wf_train = waveform( config["waveform_model"]["parameter"])
wf_valid = waveform( config["waveform_model"]["parameter"])

print( f"read train data\t{args.tdata}")
wf_train.readdata( [ args.tdata])
print( f"read valid data\t{args.vdata}")
wf_valid.readdata( [ args.vdata])

#print( wf_train.data["S"].shape)
#print( wf_valid.data["S"].shape)

# create directory 
dirname = os.path.basename(args.configure).split(".")[0]
createdir = f"{os.path.dirname(args.configure)}/{dirname}"
print( createdir)
if not os.path.exists( createdir):
	os.mkdir( createdir)
os.chdir( createdir)

import numpy as np
import matplotlib.pyplot as plt
from sklearn import preprocessing

preprocessing_scaler = waveform_module.preprocessing_scaler
preprocessing_batch = waveform_module.preprocessing_batch

scaler_train = preprocessing_scaler( wf_train, config['preprocessing'])
scaler_valid = preprocessing_scaler( wf_valid, config['preprocessing'])

batch_data = wf_train.random_batch( batch=1)
scaled_train_data = batch_data.copy()
scaled_valid_data = batch_data.copy()
preprocessing_batch( scaled_train_data, scaler_train)
preprocessing_batch( scaled_valid_data, scaler_valid)

for key in batch_data.keys():
	plt.figure( figsize=(16,9))
	if batch_data[key].dtype == np.complex64:
		plt.plot( batch_data[key][0].real, label="freq-domain-real")
		plt.plot( batch_data[key][0].imag, label="freq-domain-imag")
		plt.xlim( left=10)
	else:
		plt.plot( batch_data[key][0])
	plt.legend()
	plt.title( key)
	plt.xlabel("freq")
	plt.savefig( f"{key}_origin.png")
	plt.clf()

for key in batch_data.keys():
	print( np.sum( scaled_train_data[key][0]**2))
	plt.figure( figsize=(16,9))
	fig , ax = plt.subplots()
	ax.plot( scaled_train_data[key][0], label="train")
	ax.plot( scaled_valid_data[key][0], label="valid")

	plt.legend()
	ax2=ax.twinx()
	ax2.plot( scaled_train_data[key][0] - scaled_valid_data[key][0], label="diff")
	plt.legend()
	plt.title( key)
	plt.savefig( f"{key}.png")
	plt.clf()

#exit()


import tensorflow as tf
#tf.config.experimental.enable_tensor_float_32_execution(False)
mirrored_strategy = tf.distribute.MirroredStrategy()

#	Create model
tf_model = importlib.import_module(config["cvae_model"]["use_model"])
with mirrored_strategy.scope():
	model = tf_model.CVAE( config["cvae_model"])


#	load latest weights
wfile = glob.glob( "./tmp_weights.h5")
wfile.sort(key=os.path.getmtime)
print( wfile)


#	Plot network
import tensorflow as tf
#for key in config["cvae_model"]["network"].keys():
#	net = getattr( model, key)
#	tf.keras.utils.plot_model( net, to_file=f"{key}.png", show_shapes=True)



#	*	*	*	**	*	*	**	*	*	**	*	*	*
#	Create optimizer
with mirrored_strategy.scope():
	schedules = getattr( tf.keras.optimizers.schedules, config["schedules"]["method"])
	optimizer = getattr( tf.keras.optimizers, config["optimizer"]["method"])
	lr_schedules = schedules( **config["schedules"]["args"])
	optimizers = optimizer( learning_rate=lr_schedules)


#	annealing function 
factor	= config["KL_annealing"]["factor"]
cycle	= config["KL_annealing"]["cycle"]
nbatch	= config["KL_annealing"]["nbatch"]
def annealing( step, factor=factor, cycle=cycle):
	beta = 1
	if step < len(factor)*cycle*nbatch and step > 0:
		beta = factor[(int(step/nbatch))%len(factor)]*(1-np.cos(np.pi*float(step%nbatch)/nbatch))/2
	return np.float32(beta)
#	*	*	*	**	*	*	**	*	*	**	*	*	*

training = tf_model.training
#	Training loop

@tf.function
def distribution_train_step(optimizers, model, batch_data, annealing):
	per_replica_logpx, per_replica_kl = mirrored_strategy.run( training, args=(optimizers, model, batch_data, annealing,))
	return mirrored_strategy.reduce( tf.distribute.ReduceOp.MEAN, per_replica_logpx, axis=None), mirrored_strategy.reduce( tf.distribute.ReduceOp.MEAN, per_replica_kl, axis=None)

batch = 4096
sub_batch = 1024

if wfile:
	model.built = True
	model.load_weights( wfile[-1])
	print(f"Read weights\t:\t{wfile[-1]}")
	batch_data = wf_train.random_batch( batch=batch)
	preprocessing_batch( batch_data, scaler_valid)
	logpx_v, kl_div_v = model.loss( batch_data)
	batch_data = wf_train.random_batch( batch=1)
	preprocessing_batch( batch_data, scaler_valid)
	entropy = model.crossentropy( batch_data).numpy()
	print( logpx_v.numpy(), kl_div_v.numpy(), entropy)

batch_data = wf_train.random_batch( batch=batch)
preprocessing_batch( batch_data, scaler_train)

Ndata = len(wf_train.data["X"])
#Ndata = 10000
epochs = 400

import time


#def annealing( step, iteration=int(Ndata/batch)*5):
#	return np.float32(step/iteration)

from tqdm import trange

loss = open("loss.txt","a")

total_generation = 1000000

t0 = time.perf_counter()
for iteration in trange(1, total_generation+1):

	sub_batch_data = wf_train.random_batch( batch=sub_batch)
	preprocessing_batch( sub_batch_data, scaler_train)
	rdx = np.random.randint(batch,size=sub_batch)
	for key in batch_data.keys():
		batch_data[key][rdx] = sub_batch_data[key]

	#logpx, kl_div = training( optimizers, model, batch_data, annealing(iteration))
	logpx, kl_div = distribution_train_step( optimizers, model, batch_data, annealing(iteration))

	if iteration%50 == 0:
		valid_batch_data = wf_train.random_batch( batch=batch)
		preprocessing_batch( valid_batch_data, scaler_valid)
		logpx_v, kl_div_v = model.loss( valid_batch_data)
		valid_batch_data = wf_train.random_batch( batch=1)
		preprocessing_batch( valid_batch_data, scaler_valid)
		entropy = model.crossentropy( valid_batch_data).numpy()
		print( logpx.numpy(), kl_div.numpy(), logpx_v.numpy(), kl_div_v.numpy(), entropy)
		loss.write( f"{logpx.numpy()}\t{kl_div.numpy()}\t{logpx_v.numpy()}\t{kl_div_v.numpy()}\t{entropy}\n")

		t1 = time.perf_counter()
		times = (t1-t0)/iteration
		print( f"time\t:\t{times}")
		
		if iteration%int(500*10) == 0:
			model.save_weights("./tmp_weights.h5")

	#plt.plot( y[0])
	#plt.show()
	#break

loss.close()

model.save_weights(output)
