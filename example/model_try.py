# CVAE model with same DNN networks on Y.
import argparse

parser = argparse.ArgumentParser(description="training code")
parser.add_argument('--test', action='store_true')
parser.add_argument('--train', action='store_true')
args = parser.parse_args()


import numpy as np
xdim = 1
ydim = 1
zdim = 4
ldim = 32

scale = 1
def func(x):
	x /= scale
	x = np.repeat( x, ydim, axis=1)
	x -= scale/2
	return np.cos(2*np.pi*x) +np.random.normal(scale=np.abs(x/5))

def randomX(N=1000):
	return (np.random.rand(N)*scale).reshape(-1,xdim)
x = randomX()
y = func(x)
print( func(x).shape)
#print( x, func(x))
import matplotlib.pyplot as plt

#plt.plot( np.repeat(x,ydim), func(x),".")
#plt.show()
#
#exit()
from sklearn import preprocessing
x_scale = preprocessing.MaxAbsScaler().fit(x)
y_scale = preprocessing.StandardScaler().fit(y)

import tensorflow as tf
import tensorflow_probability as tfp

tfkl = tf.keras.layers
tfpl = tfp.layers

class CVAE(tf.keras.Model):
	def __init__(self, xdim, ydim, zdim):
		super(CVAE, self).__init__()
		print("Create CVAE")

		X = tfkl.Input( shape=xdim)
		Y = tfkl.Input( shape=ydim)
		Z = tfkl.Input( shape=zdim)

		# networks for y
		DNN_Y = tfkl.Dense( units=ldim, kernel_initializer="GlorotNormal", activation="relu")(Y)
		#DNN_Y = Y


		# p(z|x,y)
		Cn_XY = tfkl.Concatenate( axis=1)([X,DNN_Y])
		DNN_XY = tfkl.Dense( units=ldim, kernel_initializer="GlorotNormal", activation="relu" )(Cn_XY)
		DNN_XY = tfkl.Dense( units=2*zdim, kernel_initializer="GlorotNormal" )(DNN_XY)
		dist_XY = tfpl.IndependentNormal( event_shape=zdim)( DNN_XY)

		self.E1 = tf.keras.Model( [X,Y], dist_XY)
		# 


		# p(z|y)
		DNN_pY = tfkl.Dense( units=ldim, kernel_initializer="GlorotNormal", activation="relu" )(DNN_Y)
		num_components = 8
		num_parameters = int(tfpl.MixtureNormal.params_size(num_components, zdim))
		DNN_pY = tfkl.Dense( units=num_parameters, kernel_initializer="GlorotNormal" )(DNN_pY)
		dist_Y = tfpl.MixtureNormal(  num_components=num_components, event_shape=zdim)( DNN_pY)

		self.E2 = tf.keras.Model( Y, dist_Y)
		# 

		# p(x|z,y)
		Cn_ZY = tfkl.Concatenate( axis=1)([Z,DNN_Y])
		DNN_ZY = tfkl.Dense( units=ldim, kernel_initializer="GlorotNormal", activation="relu" )(Cn_ZY)
		DNN_ZY = tfkl.Dense( units=2*xdim, kernel_initializer="GlorotNormal" )(DNN_ZY)
		dist_ZY = tfpl.IndependentNormal( event_shape=xdim)( DNN_ZY)

		self.D1 = tf.keras.Model( [Z,Y], dist_ZY)
		# 

	@tf.function
	def loss(self, X, Y):
		
		P_XY = self.E1( [X,Y])
		P_Y = self.E2( Y)

		Z = P_XY.sample()

		P_YZ = self.D1( [Z,Y])

		logpx = P_YZ.log_prob( X)
		kl_div = tfp.monte_carlo.expectation(
				f=lambda x: P_XY.log_prob(x) - P_Y.log_prob(x),
				samples=P_XY.sample(),
				log_prob=P_XY.log_prob,
				)

		return -tf.reduce_mean( logpx), tf.reduce_mean( kl_div)

	@tf.function
	def crossentropy(self, Y):
		
		P_Y = self.E2( Y)

		Z = P_Y.sample()

		P_YZ = self.D1( [Z,Y])

		logpx = P_YZ.log_prob( X)

		return -tf.reduce_mean( logpx)

	@tf.function
	def test(self, Y, N=2):
		P_Y = self.E2( Y)

		Y_repeat = tf.repeat( Y, repeats=N, axis=0)
		Z = tf.reshape(P_Y.sample(N), [-1,zdim])

		P_YZ = self.D1( [Z,Y_repeat])

		return tf.reshape( P_YZ.sample(), [-1,N,xdim])


@tf.function
def training( optimizer, model, X, Y, beta):

	with tf.GradientTape() as tape:
		logpx, kl_div = model.loss( X, Y)
		#loss = logpx + kl_div * beta
		if -kl_div/logpx < 1./20 and beta < 1./4:
			loss = logpx
		else:
			loss = logpx + kl_div

		gradients = tape.gradient( loss, model.trainable_variables)

	# gradients cutoff
	#gradients = [(tf.clip_by_value(grad, -1.0, 1.0))
	#		for grad in gradients]
	optimizer.apply_gradients(zip(gradients, model.trainable_variables))
	return logpx, kl_div

# KL annealing function
KL_annealing= dict( factor=[ 1.e-4, 1/2, 1, 1, ], nbatch=500, cycle=1,)
factor	= KL_annealing["factor"]
cycle	= KL_annealing["cycle"]
nbatch	= KL_annealing["nbatch"]
def annealing( step, iteration):
	return np.float32(step/iteration)
#def annealing( step, factor=factor, cycle=cycle):
#	beta = 1
#	if step < len(factor)*cycle*nbatch:
#		beta = factor[(int(step/nbatch))%len(factor)]*(1-np.cos(np.pi*float(step%nbatch)/nbatch))/2
#	return np.float32(beta)

# learning schedule
lr_schedules = tf.keras.optimizers.schedules.ExponentialDecay( 
		initial_learning_rate=0.004, decay_steps=5000, decay_rate=.5, staircase=True)
optimizers = tf.keras.optimizers.Adam( lr_schedules)

# create CVAE model
model = CVAE( xdim, ydim, zdim)

if args.train:
	batch = 512
	iteration = 10000
	arr = np.zeros([iteration,3])
	for i in range(iteration):
	
		X = x_scale.transform( randomX(batch)).astype(np.float32)
		Y = y_scale.transform( func(X)).astype(np.float32)
	
	
		logpx, kl_div = training( optimizers, model, X, Y, annealing(i, iteration))
		if i%100:
			lp, kl, L = (logpx.numpy(), kl_div.numpy(), logpx.numpy()+ kl_div.numpy())
			cs = model.crossentropy( Y).numpy()
			a = "annealing" if -kl/lp < 1./2 else ""
			print( "%f\t%f\t%f\t%f\t%s" % ( lp, kl, L, cs, a))
		arr[i] = [logpx.numpy(), kl_div.numpy(), annealing(i, iteration)]
	
	plt.plot( arr[:,0]+arr[:,1], label="total")
	for i, label in enumerate(["logpx","kl_div","beta"]): 
		plt.plot( arr[:,i], label=label)
	#plt.ylim( -5, 5)
	plt.legend()
	plt.show()
	model.save_weights("weights.h5")

if args.test:
	model.built = True
	model.load_weights("weights.h5")
	print(f"Read weights")
	test_x = randomX(10000).astype(np.float32)
	test_y = func(test_x).astype(np.float32)
	cvae_x = model.test( y_scale.transform( test_y).astype(np.float32), 1).numpy()
	cvae_x = x_scale.inverse_transform( cvae_x[:,0,:])
	#print( cvae_x)
	#print( test_y)
	plt.plot( test_x, test_y, ".", label="func")
	plt.plot( cvae_x, test_y, ".", label="cvae")
	plt.xlabel("X")
	plt.ylabel("Y")
	plt.legend()
	plt.show()
	
