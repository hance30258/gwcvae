# This is the KL annealing example code on CVAE.
# In this code we approximate the posterior, p(x|y)m while y = func(x).
# Run it directly and change KL_annealing paramters to see how annealing affect the training loss.
# Annealing parameters matter a lot which need fine tune from cases to cases.
# But the main idea is to make logpx as small as possible in early training period.
# In my personal practice, the logpx in annealing period should smaller than the one in final period,
# which means the label x directly pass through the latant space and go into the decoder 
# without traped by KL divergnece term.


import numpy as np
xdim = 1
ydim = 1
zdim = 4
ldim = 32

scale = 1
def func(x):
	x /= scale
	x = np.repeat( x, ydim, axis=1)
	return x*5 +np.random.normal(scale=np.abs(x/5))

def randomX(N=1000):
	return (np.random.rand(N)*scale).reshape(-1,xdim)
x = randomX()
y = func(x)
print( func(x).shape)
#print( x, func(x))
import matplotlib.pyplot as plt

#plt.plot( np.repeat(x,ydim), func(x),".")
#plt.show()
#
#exit()
from sklearn import preprocessing
x_scale = preprocessing.MaxAbsScaler().fit(x)
y_scale = preprocessing.StandardScaler().fit(y)

import tensorflow as tf
import tensorflow_probability as tfp

tfkl = tf.keras.layers
tfpl = tfp.layers

class CVAE(tf.keras.Model):
	def __init__(self, xdim, ydim, zdim):
		super(CVAE, self).__init__()
		print("Create CVAE")

		self.E1 = tf.keras.Sequential([
				tfkl.Input( shape=ydim+xdim),
				tfkl.Dense( units=2*zdim, kernel_initializer="GlorotNormal" ),
				tfpl.IndependentNormal( event_shape=zdim),
			])
		self.E2 = tf.keras.Sequential([
				tfkl.Input( shape=ydim),
				tfkl.Dense( units=2*zdim, kernel_initializer="GlorotNormal" ),
				tfpl.IndependentNormal( event_shape=zdim),
			])
		self.D1 = tf.keras.Sequential([
				tfkl.Input( shape=zdim+ydim),
				tfkl.Dense( units=2*xdim, kernel_initializer="GlorotNormal"),
				tfpl.IndependentNormal( event_shape=xdim),
			])

	@tf.function
	def loss(self, X, Y):
		
		P_XY = self.E1( tf.concat([X,Y], axis=1))
		P_Y = self.E2( Y)

		Z = P_XY.sample()

		P_YZ = self.D1( tf.concat([Z,Y], axis=1))

		logpx = P_YZ.log_prob( X)
		kl_div = tfp.distributions.kl_divergence( P_XY, P_Y)

		return -tf.reduce_mean( logpx), tf.reduce_mean( kl_div)

	@tf.function
	def test(self, Y, N=2):
		P_Y = self.E2( Y)

		Y_repeat = tf.repeat( Y, repeats=N, axis=0)
		Z = tf.reshape(P_Y.sample(N), [-1,zdim])

		P_YZ = self.D1( tf.concat([Z,Y_repeat], axis=1))

		return tf.reshape( P_YZ.sample(), [-1,N,xdim])


@tf.function
def training( optimizer, model, X, Y, beta):

	with tf.GradientTape() as tape:
		logpx, kl_div = model.loss( X, Y)
		loss = logpx + kl_div * beta
		gradients = tape.gradient( loss, model.trainable_variables)

	# gradients cutoff
	#gradients = [(tf.clip_by_value(grad, -1.0, 1.0))
	#		for grad in gradients]
	optimizer.apply_gradients(zip(gradients, model.trainable_variables))
	return logpx, kl_div

# KL annealing function
KL_annealing= dict( factor=[ 1.e-2, 1/2, 1, 1, ], nbatch=4000, cycle=1,)
factor	= KL_annealing["factor"]
cycle	= KL_annealing["cycle"]
nbatch	= KL_annealing["nbatch"]
def annealing( step, factor=factor, cycle=cycle):
	beta = 1
	if step < len(factor)*cycle*nbatch:
		beta = factor[(int(step/nbatch))%len(factor)]*(1-np.cos(np.pi*float(step%nbatch)/nbatch))/2
	return np.float32(beta)

# learning schedule
lr_schedules = tf.keras.optimizers.schedules.ExponentialDecay( 
		initial_learning_rate=0.01, decay_steps=5000, decay_rate=.5, staircase=True)
optimizers = tf.keras.optimizers.Adam( lr_schedules)

# create CVAE model
model = CVAE( xdim, ydim, zdim)

batch = 32
iteration = 30000
arr = np.zeros([iteration,3])
for i in range(iteration):

	X = x_scale.transform( randomX(batch)).astype(np.float32)
	Y = y_scale.transform( func(X)).astype(np.float32)


	logpx, kl_div = training( optimizers, model, X, Y, annealing(i))
	if i%100:
		print( logpx.numpy(), kl_div.numpy())
	arr[i] = [logpx.numpy(), kl_div.numpy(), annealing(i)]

plt.plot( arr[:,0]+arr[:,1], label="total")
for i, label in enumerate(["logpx","kl_div","beta"]): 
	plt.plot( arr[:,i], label=label)
plt.ylim( -5, 5)
plt.legend()
plt.show()

test_x = randomX(10000).astype(np.float32)
test_y = func(test_x).astype(np.float32)
cvae_x = model.test( y_scale.transform( test_y).astype(np.float32), 1).numpy()
cvae_x = x_scale.inverse_transform( cvae_x[:,0,:])
#print( cvae_x)
#print( test_y)
plt.plot( test_x, test_y, ".", label="func")
plt.plot( cvae_x, test_y, ".", label="cvae")
plt.xlabel("X")
plt.ylabel("Y")
plt.legend()
plt.show()

