# --- = --- = --- = --- = --- =
# configure
# --- = --- = --- = --- = --- =
import argparse
import os
parser = argparse.ArgumentParser()
parser.add_argument("-o", "--out", help="output <out>.json file", dest="out", default="cfg.json")
parser.add_argument("-d", "--dpath", help="training data path", dest="dpath", default="./data")
parser.add_argument("-w", "--wpath", help="weight bias path", dest="wpath", default="./weight")

args = parser.parse_args()

import numpy as np


fs	=	1024
dur	=	1.
flow	= 	20
GPStime	=	1242459857.5

# --- = --- = --- = --- = --- =
# Waveform Parameters
# --- = --- = --- = --- = --- =
wfp = dict(
		detector=['L1'],
		Approximant="IMRPhenomPv2",
		domain	= 'time',
		fs	= fs,
		duration= dur,
		flow	= flow,
		gpstime = GPStime-dur+0.25,
		injection=dict(
			ra	=	1.840,
			inc	=	0.0,
			pol	=	0.0,
			dec	=	-0.61879855,
			psi	=	0.0,
			phase	=	0.0,
			a_1	=	0.0,
			a_2	=	0.0,
			tilt_1	=	0.0,
			tilt_2	=	0.0,
			phi_12	=	0.0,
			phi_jl	=	0.0,
			theta_jn	=	0.0,
			geocent_time=	GPStime,
			luminosity_distance	=	1000,),
		Prior= dict(
			mass_1	    =dict( distribution="Constraint", args=dict( minimum=20, maximum=65)),
			mass_2	    =dict( distribution="Constraint", args=dict( minimum=20, maximum=65)),
			
			mass_ratio  =dict( distribution="Uniform", args=dict( minimum=0.4, maximum=1)),
			chirp_mass  =dict( distribution="Uniform", args=dict( minimum=20, maximum=65)),

			phase	    =dict( distribution="Uniform", args=dict( minimum=0, maximum=2*np.pi)),
			geocent_time=dict( distribution="Uniform", 
				args=dict( minimum=GPStime-0.1, maximum=GPStime+0.1)),
			luminosity_distance=dict( distribution="PowerLaw",
				args=dict( alpha=2, minimum=1200, maximum=2200, name='luminosity_distance',)),
			),
		random_psd=False,
		)

# Waveform parameters
waveform_model= dict(
	use_model="waveform.wf_models_o3a",
	savepath= os.path.abspath(args.dpath),
	parameter= wfp,
	)


xdim = 5
ydim = int(fs*dur)
zdim = 8
ldim = 2048

# --- = --- = --- = --- = --- =
# Neural Network	
# --- = --- = --- = --- = --- =

# The neural network are constructed by the following dictionary
# each layer with the same group
layer_E1 = dict(
		Y	=dict( group=0, net="Input", args=dict( shape=ydim)),
		f_Y	=dict( group=0, net="Flatten", args=dict()),
		X	=dict( group=1, net="Input", args=dict( shape=xdim)),
		XY	=dict( group=[0,1], net="Concatenate", args=dict( axis=1)),
		FC1	=dict( group=2, net="Dense", args=dict( units=ldim, activation="relu", kernel_initializer="GlorotNormal")),
		FC2	=dict( group=2, net="Dense", args=dict( units=ldim, activation="relu", kernel_initializer="GlorotNormal")),
		FC3	=dict( group=2, net="Dense", args=dict( units=ldim, activation="relu", kernel_initializer="GlorotNormal")),
		diagonal_FC	=dict( group=2, net="Dense", args=dict( units=2*zdim, kernel_initializer="GlorotNormal")),
		diagonal	=dict( group=2, net="IndependentNormal", split=None, output=None, args=dict( event_shape=zdim)),
		)

import tensorflow_probability as tfp
tfpl = tfp.layers
layer_E2= dict(
		Y	=dict( group=0, net="Input", args=dict( shape=ydim)),
		f_Y	=dict( group=0, net="Flatten", args=dict()),
		FC1	=dict( group=0, net="Dense", args=dict( units=ldim, activation="relu", kernel_initializer="GlorotNormal")),
		FC2	=dict( group=0, net="Dense", args=dict( units=ldim, activation="relu", kernel_initializer="GlorotNormal")),
		FC3	=dict( group=0, net="Dense", args=dict( units=ldim, activation="relu", kernel_initializer="GlorotNormal")),
		diagonal_FC	=dict( group=0, net="Dense", args=dict( units=2*zdim, kernel_initializer="GlorotNormal")),
		diagonal	=dict( group=0, net="IndependentNormal", split=None, output=None, args=dict( event_shape=zdim)),
		)
layer_D1= dict(
		Y	=dict( group=0, net="Input", args=dict( shape=ydim)),
		f_Y	=dict( group=0, net="Flatten", args=dict()),
		Z	=dict( group=1, net="Input", args=dict( shape=zdim)),
		ZY	=dict( group=[0,1], net="Concatenate", args=dict( axis=1)),
		FC1	=dict( group=2, net="Dense", args=dict( units=ldim, activation="relu", kernel_initializer="GlorotNormal")),
		FC2	=dict( group=2, net="Dense", args=dict( units=ldim, activation="relu", kernel_initializer="GlorotNormal")),
		FC3	=dict( group=2, net="Dense", args=dict( units=ldim, activation="relu", kernel_initializer="GlorotNormal")),
		diagonal_FC	=dict( group=2, net="Dense", args=dict( units=2*xdim, kernel_initializer="GlorotNormal")),
		diagonal	=dict( group=2, net="IndependentNormal", split=None, output=None, args=dict( event_shape=xdim)),
		)


# model network
cvae_model= dict(
		use_model="cvae.tf_model",
		savepath= os.path.abspath(args.wpath),
		network= dict( 
			E1=dict(layer=layer_E1),
			E2=dict(layer=layer_E2),
			D1=dict(layer=layer_D1),),
		)

config = dict(
		# Waveform parameters
		waveform_model=waveform_model,

		# model network
		cvae_model=cvae_model,

		# training data preprocessing
		preprocessing= dict(
			X=dict( method="MinMaxScaler", args=dict()),
			Y=dict( method="StandardScaler", args=dict(with_mean=False)),
			),

		# optimizer
		optimizer= dict( method="Adam"),

		# learning rate schedule
		schedules= dict( method="ExponentialDecay", 
			args=dict( initial_learning_rate=0.0001, decay_steps=200000, decay_rate=.5, staircase=False,),),

		# KL annealing
		KL_annealing= dict( factor=[ 1.e-2, 1./4, 2./4, 1, 1, ], nbatch=7000, cycle=1,),

		)


print( f"out\t:\t{args.out}")

import json
with open( args.out,"w") as jfile:
	json.dump( config, jfile, indent=2)

