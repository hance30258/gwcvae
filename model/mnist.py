# --- = --- = --- = --- = --- =
# configure
# --- = --- = --- = --- = --- =
import argparse
import os
parser = argparse.ArgumentParser()
parser.add_argument("-o", "--out", help="output <out>.json file", dest="out", default="cfg.json")
parser.add_argument("-d", "--dpath", help="training data path", dest="dpath", default="./data")
parser.add_argument("-w", "--wpath", help="weight bias path", dest="wpath", default="./weight")

args = parser.parse_args()

import numpy as np

# --- = --- = --- = --- = --- =
# Waveform Parameters
# --- = --- = --- = --- = --- =

# Waveform parameters
waveform_model= dict(
		use_model="waveform.mnist_models",
		parameter= dict(
			# training data preprocessing
			preprocessing= dict(
				X=dict( method="MinMaxScaler", args=dict()),
				Y=dict( method="MinMaxScaler", args=dict()),),
			),
		)


ydim = 10
xdim = [28,28,1]
zdim = 2
ldim = 512

# --- = --- = --- = --- = --- =
# Neural Network	
# --- = --- = --- = --- = --- =

# The neural network are constructed by the following dictionary
# each layer with the same group
layer_E1 = dict(
		X	=dict( group=0, net="Input", args=dict( shape=xdim)),
		Cv2D1	=dict( group=0, net="Conv2D", args=dict( filters=32, kernel_size=3, strides=(2, 2), activation='relu')),
		Cv2D2	=dict( group=0, net="Conv2D", args=dict( filters=64, kernel_size=3, strides=(2, 2), activation='relu')),
		Flat	=dict( group=0, net="Flatten", args=dict()),
		Y	=dict( group=1, net="Input", args=dict( shape=ydim)),
		XY	=dict( group=[0,1], net="Concatenate", args=dict( axis=1)),
		diagonal_FC	=dict( group=2, net="Dense", args=dict( units=2*zdim, kernel_initializer="GlorotNormal")),
		diagonal	=dict( group=2, net="IndependentNormal", split=None, output=None, args=dict( event_shape=zdim)),
		)

layer_E2= dict(
		Y	=dict( group=0, net="Input", args=dict( shape=ydim)),
		FC	=dict( group=0, net="Dense", args=dict( units=7*7*32, activation="relu")),
		Flat	=dict( group=0, net="Flatten", args=dict()),
		diagonal_FC	=dict( group=0, net="Dense", args=dict( units=2*zdim, kernel_initializer="GlorotNormal")),
		diagonal	=dict( group=0, net="IndependentNormal", split=None, output=None, args=dict( event_shape=zdim)),
		)

layer_D1= dict(
		Z	=dict( group=0, net="Input", args=dict( shape=zdim)),
		Y	=dict( group=1, net="Input", args=dict( shape=ydim)),
		YZ	=dict( group=[0,1], net="Concatenate", args=dict( axis=1)),
		FC	=dict( group=2, net="Dense", args=dict( units=7*7*32, activation="relu")),
		rshape	=dict( group=2, net="Reshape", args=dict( target_shape=(7, 7, 32))),
		Cv2DT1	=dict( group=2, net="Conv2DTranspose",
			args=dict( filters=64, kernel_size=3, strides=(2, 2), padding='same', activation='relu')),
		Cv2DT2	=dict( group=2, net="Conv2DTranspose",
			args=dict( filters=32, kernel_size=3, strides=(2, 2), padding='same', activation='relu')),
		Cv2DT3	=dict( group=2, net="Conv2DTranspose",
			args=dict( filters=1, kernel_size=3, strides=1, padding='same')),
		Flat	=dict( group=2, net="Flatten", args=dict()),
		Bernoulli   =dict( group=2, net="IndependentBernoulli", split=None, output=None, args=dict(event_shape=xdim,)),
		)


# model network
cvae_model= dict(
		use_model="cvae.tf_model",
		network= dict( 
			E1=dict(layer=layer_E1),
			E2=dict(layer=layer_E2),
			D1=dict(layer=layer_D1),),
		)

config = dict(
		# Waveform parameters
		waveform_model=waveform_model,

		# model network
		cvae_model=cvae_model,


		# optimizer
		optimizer= dict( method="Adam"),

		# learning rate schedule
		schedules= dict( method="ExponentialDecay", 
			args=dict( initial_learning_rate=0.0001, decay_steps=65000, decay_rate=.5, staircase=False,),),

		# KL annealing
		KL_annealing= dict( factor=[ 1.e-2, 1./4, 2./4, 1, 1, ], nbatch=70, cycle=1,),

		)


print( f"out\t:\t{args.out}")

import json
with open( args.out,"w") as jfile:
	json.dump( config, jfile, indent=2)
