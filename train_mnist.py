#	*	*	*	*	*	*	*	*	
#	Training code
#	*	*	*	*	*	*	*	*	

import argparse
import glob, os
import json

parser = argparse.ArgumentParser(description="training code")
parser.add_argument('configure', help="configure file")
parser.add_argument("-t", "--training-data", help="training data", dest="tdata")
parser.add_argument("-v", "--validation-data", help="validation data", dest="vdata")
parser.add_argument("-o", "--output", help="final weight output", dest="output", default="final_weight.h5")
args = parser.parse_args()
print(f"cfg:\t{args.configure}")

output = os.path.abspath( args.output)
#	Read configure file
with open( args.configure,"r") as jfile:
	config = json.load( jfile)


#	Create model
import importlib
tf_model = importlib.import_module(config["cvae_model"]["use_model"])
model = tf_model.CVAE( config["cvae_model"])
#model.save_weights("./tmp_weights.h5")
#model.load_weights("./tmp_weights.h5")
#
#exit()

# import module
waveform_module = importlib.import_module(config["waveform_model"]["use_model"])

waveform = waveform_module.waveform
wf_train = waveform( config["waveform_model"]["parameter"])
wf_valid = waveform( config["waveform_model"]["parameter"])

print( f"read train data\t{args.tdata}")
wf_train.readdata( [ args.tdata])
print( f"read valid data\t{args.vdata}")
wf_valid.readdata( [ args.vdata])

#print( wf_train.data["S"].shape)
#print( wf_valid.data["S"].shape)

# create directory 
dirname = os.path.basename(args.configure).split(".")[0]
createdir = f"{os.path.dirname(args.configure)}/{dirname}"
print( createdir)
if not os.path.exists( createdir):
	os.mkdir( createdir)
os.chdir( createdir)

import numpy as np
import matplotlib.pyplot as plt

wf_train.preprocessing_scaler()
wf_valid.preprocessing_scaler()

batch_data = wf_train.random_batch( batch=1)
scaled_train_data = batch_data.copy()
scaled_valid_data = batch_data.copy()
wf_train.preprocessing_batch( scaled_train_data)
wf_valid.preprocessing_batch( scaled_valid_data)

for data in [scaled_train_data, scaled_valid_data]:


	plt.subplot(1,2,1)
	plt.figure( figsize=(16,9))
	plt.imshow( batch_data["X"][0,:,:,0])
	plt.colorbar()
	plt.legend()
	plt.xlabel("freq")
	plt.savefig( f"X_origin.png")
	plt.clf()

#exit()



#	load latest weights
wfile = glob.glob( "./tmp_weights.h5")
wfile.sort(key=os.path.getmtime)
print( wfile)
if wfile:
	model.built = True
	model.load_weights( wfile[-1])
	print(f"Read weights\t:\t{wfile[-1]}")


#	Plot network
import tensorflow as tf
for key in config["cvae_model"]["network"].keys():
	net = getattr( model, key)
	tf.keras.utils.plot_model( net, to_file=f"{key}.png", show_shapes=True)



#	*	*	*	**	*	*	**	*	*	**	*	*	*
#	Create optimizer
schedules = getattr( tf.keras.optimizers.schedules, config["schedules"]["method"])
optimizer = getattr( tf.keras.optimizers, config["optimizer"]["method"])
lr_schedules = schedules( **config["schedules"]["args"])
optimizers = optimizer( learning_rate=lr_schedules)


#	annealing function 
factor	= config["KL_annealing"]["factor"]
cycle	= config["KL_annealing"]["cycle"]
nbatch	= config["KL_annealing"]["nbatch"]
def annealing( step, factor=factor, cycle=cycle):
	beta = 1
	if step < len(factor)*cycle*nbatch:
		beta = factor[(int(step/nbatch))%len(factor)]*(1-np.cos(np.pi*float(step%nbatch)/nbatch))/2
	return np.float32(beta)
#	*	*	*	**	*	*	**	*	*	**	*	*	*

training = tf_model.training
#	Training loop

batch = 512
Ndata = len(wf_train.data["X"])
epochs = 300

import time


from tqdm import trange

loss = open("loss.txt","a")

total_generation = 1000000

t0 = time.perf_counter()
for iteration in trange(1,epochs*int(Ndata/batch)+1):

	batch_data = wf_train.random_batch( batch=batch)
	wf_train.preprocessing_batch( batch_data)

	logpx, kl_div = training( optimizers, model, batch_data, annealing(iteration))

	if iteration%100 == 1:
		batch_data = wf_valid.random_batch( batch=batch)
		wf_valid.preprocessing_batch( batch_data)
		logpx_v, kl_div_v = model.loss( batch_data)
		print( logpx.numpy(), kl_div.numpy(), logpx_v.numpy(), kl_div_v.numpy())
		loss.write( f"{logpx.numpy()}\t{kl_div.numpy()}\t{logpx_v.numpy()}\t{kl_div_v.numpy()}\n")

		t1 = time.perf_counter()
		times = (t1-t0)/iteration
		print( f"time\t:\t{times}")
		
	if iteration%int(Ndata/batch) == 1:
		model.save_weights("./tmp_weights.h5")

	#plt.plot( y[0])
	#plt.show()
	#break

loss.close()

model.save_weights(output)
