
opath = model/
all_cfg = $(wildcard $(opath)*.py)
cfg = model/new_mixnormal_o3a_psd.py
name	= $(basename $(notdir $(cfg)))


path	= ../build/

weight	= $(addprefix $(path), $(addsuffix .weight.h5, $(name)))
json	= $(addprefix $(path), $(addsuffix .json, $(name)))
data	= $(addprefix $(path), $(addsuffix .train.h5, $(name)))

num	= $(shell seq -w 108)
cache	= $(foreach n, $(name), $(addprefix $(path)$(n).cache, $(addsuffix .h5, $(num))))
.INTERMEDIATE: $(cache)

num_pe	= $(shell seq -w 256 512)
pe	= $(foreach n, $(name), $(addprefix $(path)$(n).pe, $(addsuffix .h5, $(num_pe))))


.PHONY: all clean
all: $(weight) $(pe)

pe: $(pe)

weight: $(weight)

print:
	echo $(all_cfg)

clean:
	-rm $(weight) $(data) $(json) $(cache)

.SECONDEXPANSION:

$(pe): %.h5: $$(basename %).json
	python generate_data.py --pe $(notdir $(basename $@)) $< $@

$(weight): %.weight.h5: %.json %.train.h5
	python train.py $< -o $@ -t $(word 2, $^) -v $(basename $(basename $@)).valid.h5

$(data): %.train.h5: %.json $$(addprefix %.cache, $$(addsuffix .h5, $(num)))
	python split_data.py $< -d $@ -t $(basename $(basename $@)).valid.h5 -i $(filter %.h5, $^)

$(cache): %.h5: $$(basename %).json
	python generate_data.py $^ $@

$(json): $(path)%.json: $(opath)%.py | $(path)
	python $< -o $@

$(path):
	mkdir -p $@
