import numpy as np
import bilby
import pycbc.noise
import pycbc.psd
#import matplotlib.pyplot as plt
#import matplotlib
import h5py as h5
from tqdm import tqdm, notebook


class waveform():
	def __init__(self, config):
		print("Create Waveform class")
		# set hyper parameters
		for key in config.keys():
			setattr( self, key, config[key])

		self.data = dict()

	def savedata(self, fname):

		def print_dict( group, dic):
			for key, values in dic.items():
				if isinstance( values, dict):
					sub_group = group.create_group(key)
					print_dict( sub_group, values)
				else:
					group[key] = values

		with h5.File(fname,"a") as f:
			meta = f.create_group('prior')
			print_dict( meta, self.Prior)

			data = f.create_group('data')
			for key in self.data.keys():
				data[key] = self.data[key]



	def readdata(self, fname, dataset="data"):

		data = dict()
		for i, fn in tqdm(enumerate(fname)):
			with h5.File(fn,'r') as f:
				for key in f[dataset].keys():
					print( key)
					if not key in data.keys():
						dtype = f[dataset][key].dtype
						if dtype == np.complex128:
							dtype = np.complex64
						data[key] = np.repeat( np.ones( f[dataset][key][()].shape, 
							dtype=dtype), len(fname), axis=0)
					data[key][i*len(f[dataset][key]):(i+1)*len(f[dataset][key])] = f[dataset][key][()]

		for key in data.keys():
			self.data[key] = data[key]
