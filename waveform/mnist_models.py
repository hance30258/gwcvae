
import numpy as np
import tensorflow as tf
from sklearn import preprocessing


class waveform():
	def __init__(self, config):
		print("Create Waveform class")
		# set hyper parameters
		for key in config.keys():
			setattr( self, key, config[key])
			print( key, getattr( self, key))


		self.data = dict()
		pass

	def readdata(self, args):
		(train_images, train_labels), (test_images, test_labels) = tf.keras.datasets.mnist.load_data()

		def preprocessing( images, labels):
			images = images.reshape((images.shape[0], 28, 28, 1)) / 255.
			lab = np.zeros( [labels.shape[0], 10])
			for i in range(len(labels)):
				lab[i][labels[i]] = 1
			return images, lab
			
		train_images, train_labels = preprocessing( train_images, train_labels)
		test_images, test_labels = preprocessing( test_images, test_labels)

		self.test = dict(
				Y = test_labels,
				X = test_images,
				)
		self.data = dict(
				Y = train_labels,
				X = train_images,
				)

	def preprocessing_scaler( self, batch=512):
		data = self.random_batch( batch*100)
		scaler = dict()
		for key in self.preprocessing.keys():
			process = getattr( preprocessing, self.preprocessing[key]["method"])
			shape = data[key].shape
			print(f"{key}\t:\t{shape}")
			if len(data[key].shape) > 2:
				scaler[key] = process( **self.preprocessing[key]["args"]).fit( data[key].reshape(-1,1))
			else:
				scaler[key] = process( **self.preprocessing[key]["args"]).fit( data[key])
			print( f"parameters\t:\t{scaler[key].get_params()}")
		self.scaler = scaler

	def preprocessing_batch( self, data):
		for key in self.preprocessing.keys():
			if len(data[key].shape) > 2:
				shape = data[key].shape
				data[key] = self.scaler[key].transform( data[key].reshape(-1,1))
				data[key] = data[key].reshape(shape).astype("float32")
			else:
				data[key] = self.scaler[key].transform( data[key]).astype("float32")

	def random_batch(self, batch=512):
		idx = np.random.randint(len(self.data["X"]),size=batch)

		X = self.data["X"][idx]
		Y = self.data["Y"][idx]

		return dict( X=X, Y=Y)
