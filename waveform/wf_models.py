import numpy as np
import bilby
import pycbc.noise
import pycbc.psd
import os, requests

from waveform.wf_utils import waveform

class waveform(waveform):
	def __init__(self, config):
		super(waveform, self).__init__(config)

	def get_psd(self, gpstime):

		from gwosc.locate import get_urls
		from gwpy.timeseries import TimeSeries
		self.PSD = dict()
		for det in self.detector:
			self.PSD[det] = []
			for t0 in gpstime:
				url = get_urls(det, t0, t0)[0]
				fn = os.path.basename(url)

				if not os.path.exists(fn):
					print(f"Downloading\t{fn}")
					with open(fn,'wb') as strainfile:
						straindata = requests.get(url)
						strainfile.write(straindata.content)
				print(fn)
				print(os.path.abspath(os.getcwd()))
				strain = TimeSeries.read(fn,format='hdf5.losc')
				strain = strain.crop(t0-16, t0+16)
				strain = strain.resample(self.fs)
				#print(strain.sample_rate)
				psd = strain.psd( fftlength=self.duration)
				#psd[:int(self.flow*self.duration)] = 0
				self.PSD[det].append( psd)
	
	def get_aLIGO_psd(self):

		delta_f = 1.0 / self.duration
		flen = int(self.fs*self.duration//2)+1
		psdLIGO = pycbc.psd.aLIGOZeroDetHighPower(flen, delta_f, self.flow)

		# I have no idea why the latest value is zero. Replace by the second latest one.
		psdLIGO.data[-1] = psdLIGO.data[-2]

		from gwpy.frequencyseries import FrequencySeries
		psd = FrequencySeries( psdLIGO.data, frequencies= psdLIGO.sample_frequencies)

		self.PSD = dict()
		for det in self.detector:
			self.PSD[det] = [psd]

	
	def get_random_psd(self, scale=1./8):

		psd = dict()
		for det in self.detector:

			n_psd = len(self.PSD[det])
			if n_psd > 1:
				shuffle_index = np.arange( n_psd)
				np.random.shuffle(shuffle_index)

				psd[det] = np.zeros_like( self.PSD[det][0])
				p = 1.
				for ind in shuffle_index:
					rand = np.random.rand()
					psd[det] += self.PSD[det][ind]*rand*p
					p *= 1-rand
					if p < 0.01:
						break
				psd[det] += self.PSD[det][shuffle_index[-1]] * p
			else:
				psd[det] = self.PSD[det][0].copy()

			# Add random variation
			if scale != 0:
				psd[det] *= np.exp(-np.random.normal( scale=scale,size=len(psd[det]))) 
				#psd[det] *= np.exp(-np.random.normal(scale=scale*2))
				alpha = 1
				while np.abs(alpha-1) < 0.2:
					alpha = np.exp(-np.random.normal(scale=scale*2))
				psd[det] *= alpha

		return psd

	def prior_sample(self):
		if not hasattr( self, "PriorDict"):
			PriorDict = bilby.core.prior.PriorDict()
			for key in self.Prior.keys():
				PriorDict[key] = getattr( bilby.core.prior, self.Prior[key]["distribution"]
						)( **self.Prior[key]["args"])
			for key in PriorDict.keys():
				print( PriorDict[key], key)
			self.PriorDict = PriorDict
		from pycbc.pnutils import mchirp_q_to_mass1_mass2
		m1m2 = [0,0]
		while m1m2[0] > self.Prior['mass_1']['args']['maximum'] or m1m2[1] < self.Prior['mass_2']['args']['minimum']:
			x = self.PriorDict.sample()
			if 'mass_1' not in x:
				m1m2 = mchirp_q_to_mass1_mass2( x['chirp_mass'], x['mass_ratio'])
				print(m1m2)
			else:
				m1m2 = [x["mass_1"], x["mass_2"]]

		return x



	#def td_sample(self, n=1, domain="time"):

	#	# check psd model
	#	if not hasattr( self, "PSD"):
	#		print( "No PSD given. Use aLIGOZeroDetHighPower.")
	#		self.get_aLIGO_psd()


	#	injection = self.injection.copy()
	#	# Set prior 


	#	X= np.zeros( [n,len(self.prior_sample())], dtype=float)
	#	N= np.zeros( [n,int(self.fs*self.duration),len(self.detector)], dtype=complex)
	#	S= np.zeros( [n,int(self.fs*self.duration),len(self.detector)], dtype=complex)
	#	ASD= np.zeros( [n,int(self.fs*self.duration/2+1),len(self.detector)], dtype=float)
	#	snr= np.zeros( [n,len(self.detector)], dtype=float)

	#	self.data = dict( X=X, N=N, S=S, ASD=ASD, snr=snr)

	#	waveform_arguments = dict(
	#			waveform_approximant=self.Approximant,
	#			reference_frequency=50., 
	#			minimum_frequency=self.flow)

	#	waveform_generator = bilby.gw.WaveformGenerator(
	#			duration=self.duration, 
	#			sampling_frequency=self.fs,
	#			frequency_domain_source_model=bilby.gw.source.lal_binary_black_hole,
	#			parameter_conversion=bilby.gw.conversion.convert_to_lal_binary_black_hole_parameters,
	#			waveform_arguments=waveform_arguments)


	#	for i in range(n):

	#		# Change injection parameter
	#		prior_sample = self.prior_sample()
	#		X[i] = list(prior_sample.values())

	#		for idx, key in enumerate(prior_sample.keys()):
	#			injection[key] = prior_sample[key]
	#			print( idx, key, "->", prior_sample[key])

	#		waveform_generator.parameters = injection

	#		# psd realization
	#		if self.random_psd:
	#			psd = self.get_random_psd()
	#		else:
	#			psd = self.get_random_psd(scale=0)


	#		ifos = bilby.gw.detector.InterferometerList(self.detector)
	#		# interferometer realization
	#		for idx, det in enumerate(self.detector):

	#			psd[det] = np.nan_to_num( psd[det], nan=np.inf)
	#			psd[det][ psd[det] ==0] = np.inf
	#			#print( psd[det])
	#			#exit()
	#			ifos[idx].power_spectral_density =\
	#					bilby.gw.detector.PowerSpectralDensity.from_power_spectral_density_array(
	#							frequency_array=psd[det].frequencies,
	#							psd_array=psd[det])

	#			ifos[idx].strain_data.set_from_power_spectral_density(
	#					ifos[idx].power_spectral_density,
	#					sampling_frequency=self.fs, duration=self.duration,
	#					start_time=self.gpstime)


	#			# noise strain
	#			noise = ifos[idx].whitened_frequency_domain_strain

	#			# injection signal
	#			ifos[idx].inject_signal(waveform_generator=waveform_generator, parameters=injection)

	#			# waveform strain
	#			signal = ifos[idx].whitened_frequency_domain_strain
	#			signal -= noise


	#			from bilby.core.utils import infft

	#			# replace nan to zero
	#			noise = np.nan_to_num( noise)
	#			signal = np.nan_to_num( signal)
	#			# inverse FFT
	#			noise_td = infft( noise, self.fs)
	#			signal_td = infft( signal, self.fs)


	#			# asd
	#			asd = ifos[idx].amplitude_spectral_density_array

	#			#import matplotlib.pyplot as plt

	#			#plt.psd( noise_td, Fs= self.fs)
	#			#plt.axhline(0)
	#			#plt.show()
	#			#plt.plot( noise_td)
	#			#plt.plot( signal_td)
	#			#plt.show()
	#			#plt.plot( psd.frequencies, noise, "g")
	#			#plt.plot( psd.frequencies, asd, "b")
	#			#plt.plot( psd.frequencies, signal_response, "r")
	#			#plt.xscale("log")
	#			#plt.xlim( self.flow)
	#			#plt.show()

	#			N[i,:,idx] = noise_td
	#			S[i,:,idx] = signal_td
	#			ASD[i,:,idx] = asd

	#			for key, value in ifos[idx].meta_data.items():
	#				print( key, "->", value)
	#			snr[i,idx] = np.abs(ifos[idx].meta_data["matched_filter_SNR"])


	#	if len(self.detector) == 1:
	#		for name in ["S","N","ASD","snr"]:
	#			self.data[name] = self.data[name].reshape(n,-1)



	def sample(self, n=1, domain="frequency", pe=False):

		# check psd model
		if not hasattr( self, "PSD"):
			print( "No PSD given. Use aLIGOZeroDetHighPower.")
			self.get_aLIGO_psd()

		if pe:
			sampler = "dynesty"
			outdir = f"{sampler}_{pe}"
			bilby.core.utils.setup_logger(outdir=outdir, label=outdir)

		injection = self.injection.copy()
		# Set prior 


		X= np.zeros( [n,len(self.prior_sample())], dtype=float)
		ASD= np.zeros( [n,int(self.fs*self.duration/2+1),len(self.detector)], dtype=float)
		snr= np.zeros( [n,len(self.detector)], dtype=float)

		if self.domain == "frequency":
			N= np.zeros( [n,int(self.fs*self.duration/2+1),len(self.detector)], dtype=complex)
			S= np.zeros( [n,int(self.fs*self.duration/2+1),len(self.detector)], dtype=complex)
		elif self.domain == "time":
			N= np.zeros( [n,int(self.fs*self.duration),len(self.detector)], dtype=float)
			S= np.zeros( [n,int(self.fs*self.duration),len(self.detector)], dtype=float)


		self.data = dict( X=X, N=N, S=S, ASD=ASD, snr=snr)

		waveform_arguments = dict(
				waveform_approximant=self.Approximant,
				reference_frequency=50., 
				minimum_frequency=self.flow)

		waveform_generator = bilby.gw.WaveformGenerator(
				duration=self.duration, 
				sampling_frequency=self.fs,
				frequency_domain_source_model=bilby.gw.source.lal_binary_black_hole,
				parameter_conversion=bilby.gw.conversion.convert_to_lal_binary_black_hole_parameters,
				waveform_arguments=waveform_arguments)


		for i in range(n):

			# Change injection parameter
			prior_sample = self.prior_sample()
			# make m1 < m2
			if "mass_2" in prior_sample.keys():
				prior_sample["mass_1"], prior_sample["mass_2"] =\
						np.sort( [prior_sample["mass_1"],prior_sample["mass_2"]])
			print( prior_sample)
			X[i] = list(prior_sample.values())
			print( X[i])

			for idx, key in enumerate(prior_sample.keys()):
				injection[key] = prior_sample[key]
				print( idx, key, "->", prior_sample[key])

			#print("Injection")
			#for idx, key in enumerate(injection.keys()):
			#	print( idx, key, "->", injection[key])

			waveform_generator.parameters = injection
			## waveform frequency strain
			#signal = waveform_generator.frequency_domain_strain()

			# psd realization
			if self.random_psd:
				psd = self.get_random_psd()
			else:
				psd = self.get_random_psd(scale=0)


			ifos = bilby.gw.detector.InterferometerList(self.detector)
			# interferometer realization
			for idx, det in enumerate(self.detector):

				psd[det] = np.nan_to_num( psd[det], nan=np.inf)
				#print( type(psd[det]))
				#print( psd[det].data)
				ifos[idx].power_spectral_density =\
						bilby.gw.detector.PowerSpectralDensity.from_power_spectral_density_array(
								frequency_array=psd[det].frequencies,
								psd_array=psd[det])

				ifos[idx].strain_data.set_from_power_spectral_density(
						ifos[idx].power_spectral_density,
						sampling_frequency=self.fs, duration=self.duration,
						start_time=self.gpstime)

				## waveform strain
				#signal_response = ifos[idx].get_detector_response(signal, injection)

				if self.domain == "frequency":
					# noise strain
					noise = ifos[idx].strain_data.frequency_domain_strain

					# injection signal
					ifos[idx].inject_signal(waveform_generator=waveform_generator, parameters=injection)

					# waveform strain
					signal = ifos[idx].strain_data.frequency_domain_strain
					signal -= noise

				elif self.domain == "time":
					# noise strain
					noise_fd = ifos[idx].whitened_frequency_domain_strain

					# injection signal
					ifos[idx].inject_signal(waveform_generator=waveform_generator, parameters=injection)

					# waveform strain
					signal_fd = ifos[idx].whitened_frequency_domain_strain
					signal_fd -= noise_fd


					from bilby.core.utils import infft

					# replace nan to zero
					noise_fd = np.nan_to_num( noise_fd)
					signal_fd = np.nan_to_num( signal_fd)
					# inverse FFT
					noise = infft( noise_fd, self.fs)
					signal = infft( signal_fd, self.fs)



				# asd
				asd = ifos[idx].amplitude_spectral_density_array

				#plt.plot( psd.frequencies, noise, "g")
				#plt.plot( psd.frequencies, asd, "b")
				#plt.plot( psd.frequencies, signal_response, "r")
				#plt.xscale("log")
				#plt.xlim( self.flow)
				#plt.show()

				N[i,:,idx] = noise
				S[i,:,idx] = signal
				ASD[i,:,idx] = asd

				for key, value in ifos[idx].meta_data.items():
					print( key, "->", value)
				snr[i,idx] = np.abs(ifos[idx].meta_data["matched_filter_SNR"])

			
			if pe:
				likelihood = bilby.gw.GravitationalWaveTransient(
						interferometers=ifos, waveform_generator=waveform_generator)
				
				priors = self.PriorDict
				for key in injection.keys():
					if key not in self.Prior.keys():
						priors[key] = injection[key]

				#sampler = "ptemcee"
				# Run sampler.  In this case we're going to use the `dynesty` sampler
				result = bilby.run_sampler(
						likelihood=likelihood, priors=priors, sampler='dynesty', 
						nlive=5000, walks=30, n_check_point=10000, npool=2,
						injection_parameters=injection, outdir=outdir, label=outdir,
						save='hdf5')
				# Make a corner plot.
				result.plot_corner()
				self.result = result

		## Change mass1/mass2 to mchirp/q
		#from pycbc.transforms import Mass1Mass2ToMchirpQ
		#transform = Mass1Mass2ToMchirpQ()
		#param = transform.transform( dict(mass1=X[:,1],mass2=X[:,0]))
		#X[:,0], X[:,1] = param["q"], param["mchirp"]


		if len(self.detector) == 1:
			for name in ["S","N","ASD","snr"]:
				self.data[name] = self.data[name].reshape(n,-1)
	



	def random_batch(self,batch=1, same_seed=False, num=None):

		idx = np.random.randint(len(self.data["X"]),size=batch)
		idx_n = np.random.randint(len(self.data["N"]),size=batch)

		S = self.data["S"][idx]
		X = self.data["X"][idx]

		ASD = self.data["ASD"][idx_n]
		N = self.data["N"][idx_n]

		ASD[:,:int(self.duration*self.flow)] = 0
		# random phase
		if self.domain == 'frequency':
			phase = np.exp( 1j*np.random.random(N.shape[-1])*2*np.pi)
			N *= phase
		# time shift
		elif self.domain == 'time':
			shift = np.random.randint(len(self.data["N"][0]))
			order = np.arange(len(self.data["N"][0]))
			order -= shift
			order[:shift] += len(order)
			N = N[:,order]

		# shuffle noise
		#ind = np.arange(batch)
		#for i in range(len(ASD[0])):
		#	np.random.shuffle(ind)
		#	ASD[:,i] = ASD[ind,i]
		#	N[:,i] = N[ind,i]

		Y = S + N

		return dict( Y=Y, X=X, ASD=ASD, S=S)

from sklearn import preprocessing

def preprocessing_scaler( wf, method, batch=512):
	#data = wf.random_batch( len(wf.data["X"]))
	data = wf.random_batch( batch*100)
	scaler = dict()
	for key in method.keys():
		process = getattr( preprocessing, method[key]["method"])
		shape = data[key].shape
		print(f"{key}\t:\t{shape}")
		if data[key].dtype == np.complex64:
			data[key] = np.concatenate( [ data[key].real, data[key].imag], axis=1)
			scaler[key] = process( **method[key]["args"]).fit( data[key].reshape([-1,1]))
		else:
			scaler[key] = process( **method[key]["args"]).fit( data[key])
		print( f"parameters\t:\t{scaler[key].get_params()}")
	return scaler

def preprocessing_batch( data, scaler):
	for key in scaler.keys():
		if data[key].dtype == np.complex64:
			shape = data[key].shape
			data[key] = np.concatenate( [ data[key].real, data[key].imag], axis=1)
			data[key] = scaler[key].transform( data[key].reshape(-1,1))
			data[key] = data[key].reshape([shape[0],shape[1],2]).astype("float32")
			data[key] /= 4
		else:
			data[key] = scaler[key].transform( data[key]).astype("float32")

