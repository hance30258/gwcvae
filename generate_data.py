import argparse
import os

parser = argparse.ArgumentParser(description="dataset generate code")
parser.add_argument('--test', action='store_true')
parser.add_argument('--pe', help='pe folder number')
parser.add_argument('--gpstime', help='strain data event gpstime')
parser.add_argument('configure')
parser.add_argument('outfile')
args = parser.parse_args()
print(f"out:\t{args.outfile}")
print(f"cfg:\t{args.configure}")


fname = os.path.abspath(args.configure)

import json
with open( args.configure,"r") as jfile:
	config = json.load( jfile)

config = config["waveform_model"]

out_name =  args.outfile
print(out_name)

import importlib
waveform_module = importlib.import_module(config["use_model"])
waveform = waveform_module.waveform

wf = waveform( config["parameter"])
if hasattr( wf, "use_psd"):
	wf.get_psd( wf.use_psd)
#wf.get_psd( [
#	1242459857.5,	# GW190521_074359
#	1251009263.8,	# GW190828_063405
#	])
#wf.readdata(["./test.h5"])
#print( wf.data)
#exit()

N = 2**10*2**4
if args.test:
	N = 4000


if args.pe:
	print( f"PE ::: {args.pe}")
	wf.sample( 1, pe=args.pe)
	wf.result.save_to_file( out_name, extension='hdf5')
	wf.savedata( out_name)
else:
	print(f"Generate {N} samples waveform")
	wf.sample( N)
	wf.savedata( out_name)

# Plot data
if args.test:
	import matplotlib.pyplot as plt
	import numpy as np
	scale = np.max( np.abs(wf.data["S"][:10]))
	freq = np.linspace(0,wf.fs//2,int(wf.fs*wf.duration)//2+1)

	for i in range(10):
		plt.figure(figsize=(32,18))
		for data in [ wf.data["N"][i]+wf.data["S"][i], wf.data["S"][i], wf.data["ASD"][i]]:
			plt.plot( freq, data)
		plt.xscale("log")
		plt.ylim(-scale,scale)
		plt.xlim(wf.flow-10)
		plt.title("snr="+str(wf.data["snr"][i]))
		plt.savefig(f"figure{i}.png")
		plt.clf()

	plt.figure(figsize=(32,18))
		#plt.xscale("log")
	#plt.plot( np.linspace( 0, wf.fs/2, len(wf.PSD[wf.detector[0]])) , np.sqrt( wf.PSD[wf.detector[0]]), "b")
	for i in range(len(wf.PSD[wf.detector[0]])):
		plt.loglog( freq, np.sqrt(wf.PSD[wf.detector[0]][i]), "b")
	for i in range(10):
		plt.loglog( freq, wf.data["ASD"][i])
	#scale = np.max( np.abs(wf.data["S"][0]))
	#plt.ylim(None,scale)
	plt.xlim(wf.flow-10)
	plt.savefig("figure_psd.png")
	plt.clf()
	plt.hist( wf.data["snr"],bins=50)
	plt.savefig("figure_snr.png")
	plt.clf()
	#exit()
