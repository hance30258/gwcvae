import argparse
import json

parser = argparse.ArgumentParser(description="training code")
parser.add_argument('configure', help="configure file")
parser.add_argument("-d", "--dout", help="training data", dest="dout", default="training.h5")
parser.add_argument("-t", "--tout", help="testing data", dest="tout", default="test.h5")
parser.add_argument("-i", "--input", help="input file list", dest="input", nargs="*")

args = parser.parse_args()
print(f"cfg:\t{args.configure}")

#	Read configure file
with open( args.configure,"r") as jfile:
	config = json.load( jfile)

#	Read Training data
fn = args.input
fn.sort()
#fn = fn[:3]
print("file",fn)
#	split training set
valid_file = fn[::10].copy()
print("valid_file",valid_file)
train_file = fn.copy()
for item in valid_file:
	train_file.remove(item)

print("train_file",train_file)

# import module
import importlib
waveform_module = importlib.import_module(config["waveform_model"]["use_model"])
waveform = waveform_module.waveform
wf_train = waveform( config["waveform_model"]["parameter"])
wf_valid = waveform( config["waveform_model"]["parameter"])

wf_train.readdata( train_file)
wf_valid.readdata( valid_file)

#print( wf_train.data["S"].shape)
#print( wf_valid.data["S"].shape)

print( f"save train data {args.dout}")
wf_train.savedata( args.dout)
print( f"save valid data {args.tout}")
wf_valid.savedata( args.tout)
